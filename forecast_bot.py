from telebot.async_telebot import AsyncTeleBot
import asyncio
from pygismeteo import Gismeteo
from datetime import date
import locale
import settings


locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')

bot = AsyncTeleBot(settings.TOKEN)

def check_weather(city: str) -> str:
    gismeteo = Gismeteo()
    day = date.today().day
    month = date.today().strftime("%B")
    try:
        search_results = gismeteo.search.by_query(city)
        city_id = search_results[0].id
        current = gismeteo.current.by_id(city_id)
        temperature = current.temperature.air.c
        description = current.description.full
        water = current.temperature.water.c
        humidity = current.humidity.percent
        return f'{search_results[0].name}'\
               f'\nСегодня {day} {month}. На улице {temperature} градусов. ' \
               f'\nЗа окном: {description}' \
               f'\nВлажность: {humidity}%' \
               f'\nВода: {water} градусов' \
               
    except Exception:
        return 'Не могу найти такой населённый пункт. Попробуй ещё'
    
@bot.message_handler(commands=["start"])
async def start(m, res=True):
    await bot.send_message(m.chat.id, 'Укажи город, и узнай погоду.\nНапример: "Екат"')
    
@bot.message_handler(content_types=["text"])
async def handle_text(message):
    await bot.send_message(message.chat.id, check_weather(message.text))
    

if __name__ == "__main__":
    asyncio.run(bot.polling(none_stop=True, interval=0))
