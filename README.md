![](https://cdn.icon-icons.com/icons2/2505/PNG/256/sunny_weather_icon_150663.png)
### Телеграм бот с отображением погоды по запросу города
---

Простой вариант на библиотеке [AsyncTeleBot](https://pytba.readthedocs.io/en/latest/async_version/index.html)

~~Запущен на бесплатном облаке, возможны перебои с доступностью~~

[Ссылка на бота](https://t.me/itc_weather_forecast_bot)

UPD. Запустил скрипт на платном хостинге, запустил службу, работает без перебоев.
